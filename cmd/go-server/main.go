package main

import (
	"net/http"
	"os"

	"gitlab.com/jaime/go-server/internal/middleware"
	"go.uber.org/zap"
)

func main() {
	logger, err := logger(os.Getenv("ENVIRONMENT"))
	if err != nil {
		panic(err)
	}
	defer logger.Sync() // flushes buffer, if any

	m := http.NewServeMux()

	h := &handlers{
		logger: logger,
	}

	fn := middleware.AccessLogger(logger)
	m.Handle("/info", fn(http.HandlerFunc(h.Info)))

	addr := os.Getenv("SERVER_ADDR")
	logger.Sugar().Infow("server listening", "addr", addr)

	if err := http.ListenAndServe(addr, m); err != nil {
		logger.Sugar().With("error", err).Error("failed to listen and serve")
	}
}

type handlers struct {
	logger *zap.Logger
}

func (h *handlers) Info(w http.ResponseWriter, r *http.Request) {
	h.logger.Sugar().Info("received request")
	w.WriteHeader(http.StatusNoContent)
}

func logger(env string) (*zap.Logger, error) {
	if env == "production" {
		return zap.NewProduction()
	}

	return zap.NewDevelopment()
}
